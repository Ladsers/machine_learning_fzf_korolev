#include "NNImpl.h"

#define ANNDLL_EXPORTS

ANN::NNImpl::NNImpl(std::vector<size_t> conf, ActivationType aType, float scale)
{
	this->configuration = conf;
	this->activation_type = aType;
	this->scale = scale;
	if (!conf.empty()) RandomInit();
	is_trained = false;
}


ANN::NNImpl::~NNImpl()
{
}

std::string ANN::NNImpl::GetType()
{
	return "Neural network by Max Korolev";
}

std::vector<float> ANN::NNImpl::Predict(std::vector<float> & input)
{
	if (!is_trained) throw std::runtime_error("Network untrained / Missing network configuration file");
	if (configuration.empty()) throw std::runtime_error("Configuration is empty");
	if (input.size() != configuration[0]) throw std::runtime_error("The number of input elements does not match the parameter in the configuration");

	std::vector<float> prev_out = input;
	std::vector<float> out;
	
	for (size_t layer_idx = 0; layer_idx < configuration.size() - 1; layer_idx++){
		out.resize(configuration[layer_idx + 1],0);
		for (size_t to_idx = 0; to_idx < configuration[layer_idx + 1]; to_idx++){
			for (size_t from_idx = 0; from_idx < configuration[layer_idx]; from_idx++){
				out[to_idx] += weights[layer_idx][from_idx][to_idx] * prev_out[from_idx];
			}
			out[to_idx] = Activation(out[to_idx]);
		}
		prev_out = out;
	}

	return out;

}

std::shared_ptr<ANN::ANeuralNetwork> ANN::CreateNeuralNetwork(std::vector<size_t>& conf, ANN::ANeuralNetwork::ActivationType aType, float scale){
	return std::make_shared<NNImpl>(conf, aType, scale);
}

float ANN::BackPropTraining(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	std::vector<std::vector<float>> & inputs,
	std::vector<std::vector<float>> & outputs,
	int maxIters,
	float eps,
	float speed,
	bool std_dump)
{
	float error;
	int iter = 0;
	ann->RandomInit();
	if (inputs.size() != outputs.size()) throw std::runtime_error("Number of input elements is not equal to output");
	do
	{
		error = 0;
		for (size_t sample_idx = 0; sample_idx < inputs.size(); sample_idx++)
		{
			error += BackPropTrainingIteration(ann, inputs[sample_idx], outputs[sample_idx], speed);
		}
		error = sqrt(error);

		iter++;
		if (std_dump && iter % 100 == 0){ std::cout << iter << "/" << maxIters << "\t Error: " << error << std::endl; }
		if (error < eps) ann->is_trained = true;
	} while (error > eps && iter < maxIters);

	return error;
}

float ANN::BackPropTrainingIteration(
	std::shared_ptr<ANN::ANeuralNetwork> ann,
	const std::vector<float>& input,
	const std::vector<float>& output,
	float speed)
{
	float error = 0.0f;

	std::vector< std::vector <float>> outs(ann->configuration.size());
	outs[0] = input;
	for (size_t layer_idx = 0; layer_idx < ann->configuration.size() - 1; layer_idx++){
		outs[layer_idx + 1].resize(ann->configuration[layer_idx + 1], 0);
		for (size_t to_idx = 0; to_idx < ann->configuration[layer_idx + 1]; to_idx++){
			for (size_t from_idx = 0; from_idx < ann->configuration[layer_idx]; from_idx++){
				outs[layer_idx+1][to_idx] += ann->weights[layer_idx][from_idx][to_idx] * outs[layer_idx][from_idx];
			}
			outs[layer_idx+1][to_idx] = ann->Activation(outs[layer_idx + 1][to_idx]);
		}
	}

	std::vector < std::vector < float>> sigmas(ann->configuration.size());
	sigmas.back().resize(outs.back().size());
	for (size_t id = 0; id < output.size(); id++)
	{
		sigmas.back()[id] = (output[id] - outs.back()[id])*ann->ActivationDerivative(outs.back()[id]);
		error += (outs.back()[id] - output[id])*(outs.back()[id] - output[id]);
	}
	
	std::vector<std::vector<std::vector<float>>> dw(ann->configuration.size() - 1);
	
	for (size_t layer_idx = ann->configuration.size() - 2; layer_idx + 1 != 0; layer_idx--)
	{
		dw[layer_idx].resize(ann->weights[layer_idx].size());
		sigmas[layer_idx].resize(ann->configuration[layer_idx], 0.0f);

		for (size_t from_idx = 0; from_idx < ann->configuration[layer_idx]; from_idx++)
		{
			for (size_t to_idx = 0; to_idx < ann->configuration[layer_idx + 1]; to_idx++){
				sigmas[layer_idx][from_idx] += sigmas[layer_idx + 1][to_idx] * ann->weights[layer_idx][from_idx][to_idx];
			}
			sigmas[layer_idx][from_idx] *= ann->ActivationDerivative(outs[layer_idx][from_idx]);
			dw[layer_idx][from_idx].resize(ann->weights[layer_idx][from_idx].size());
			for (size_t to_idx = 0; to_idx < ann->configuration[layer_idx + 1]; to_idx++){
				dw[layer_idx][from_idx][to_idx] = speed * sigmas[layer_idx + 1][to_idx] * outs[layer_idx][from_idx];
				ann->weights[layer_idx][from_idx][to_idx] += dw[layer_idx][from_idx][to_idx];
			}
		}
	}

	return error;
}