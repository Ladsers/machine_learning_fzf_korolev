#pragma once
#include "ANN.h"
#include <iostream>
namespace ANN
{
	class NNImpl :
		public ANN::ANeuralNetwork
	{
	public:
		NNImpl(std::vector<size_t> conf, ActivationType aType, float scale);
		~NNImpl();

		ANNDLL_API virtual std::string GetType();
		ANNDLL_API virtual std::vector<float> Predict(std::vector<float> & input);
	};

}