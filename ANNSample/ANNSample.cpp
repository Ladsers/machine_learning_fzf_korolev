#include <iostream>
#include <ANN.h>
#include <NNImpl.h>
using namespace std;
using namespace ANN;

int main()
{
	vector <size_t> conf = { 2, 3, 3, 1 };
	auto nn = CreateNeuralNetwork(conf);

	cout << nn->GetType().c_str() << endl;

	vector<vector<float>> inputs, outputs;
	bool success = LoadData("xor.data", inputs, outputs);
	if (!success)
	{
		cout << "could not open file \"xor.data\"" << endl;
		return -1;
	}

	if (!nn->Load("xor.ann"))
	{
		cout << "could not open file \"xor.ann\"" << endl;
		return -1;
	}

	for (int i = 0; i < inputs.size(); i++){
		auto res = nn->Predict(inputs[i]);
		cout << inputs[i][0] << "\t" << inputs[i][1] << "\t" << outputs[i][0] << "\t" << res[0] << endl;
	}
	
	return 0;
}